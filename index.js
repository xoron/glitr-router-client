Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _socket = require('socket.io-client');

var _socket2 = _interopRequireDefault(_socket);

var _urlParse = require('url-parse');

var _urlParse2 = _interopRequireDefault(_urlParse);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

require('isomorphic-fetch');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var GlitrRouterClient = function () {
    function GlitrRouterClient(routes) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, GlitrRouterClient);

        var _options$namespace = options.namespace,
            namespace = _options$namespace === undefined ? '' : _options$namespace,
            _options$requestTimeo = options.requestTimeout,
            requestTimeout = _options$requestTimeo === undefined ? 10000 : _options$requestTimeo,
            _options$reactNative = options.reactNative,
            reactNative = _options$reactNative === undefined ? false : _options$reactNative;


        this.options = {
            namespace: namespace,
            requestTimeout: requestTimeout
        };

        this.routes = routes;
        this.socket = null;
        this.endpoint = '';

        // required for react-native
        if (!!options.reactNative) window.navigator.userAgent = 'ReactNative';
    }

    _createClass(GlitrRouterClient, [{
        key: 'generateSocketioRoutes',
        value: function () {
            function generateSocketioRoutes() {
                var _this = this;

                this.routes.forEach(function (route) {
                    _this.socket.on(String(route.method) + '::>' + String(route.path), _this.generateSocketHandler(route));
                });
            }

            return generateSocketioRoutes;
        }()
    }, {
        key: 'generateSocketHandler',
        value: function () {
            function generateSocketHandler(route) {
                var _this2 = this;

                return function (payload) {
                    var body = payload.body,
                        headers = payload.headers;

                    var req = {
                        headers: Object.assign({}, headers, (0, _urlParse2['default'])(route.path)),
                        body: body
                    };

                    var generateReponse = function () {
                        function generateReponse(status) {
                            return function (data) {
                                var headerProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

                                var newHeaders = Object.assign({
                                    status: status
                                }, headers, headerProps);

                                if (!!headers.callback) {
                                    _this2.socket.emit(headers.callback, { headers: newHeaders, body: data });
                                }
                            };
                        }

                        return generateReponse;
                    }();

                    var res = {
                        send: generateReponse(200),
                        end: generateReponse(200),
                        emit: generateReponse(200),
                        fail: generateReponse(400),
                        error: generateReponse(200)
                    };

                    var runHandler = function () {
                        function runHandler(index) {
                            route.handler[index](req, res, function () {
                                return runHandler(index + 1);
                            });
                        }

                        return runHandler;
                    }();

                    runHandler(0);
                };
            }

            return generateSocketHandler;
        }()
    }, {
        key: 'generateSocketEmitter',
        value: function () {
            function generateSocketEmitter(method, endpoint, payload) {
                var _this3 = this;

                var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

                return new Promise(function (resolve, reject) {
                    if (!!headers.callback) {
                        var requestTimeout = _this3.options.requestTimeout;

                        var randomHash = _crypto2['default'].randomBytes(64).toString('hex');

                        var requestTimer = setTimeout(function () {
                            reject('request timed out :(');
                        }, requestTimeout);

                        headers.callback = randomHash;
                        _this3.socket.once(randomHash, function (payload) {
                            clearTimeout(requestTimer);
                            resolve(payload);
                        });
                    }

                    _this3.socket.emit(String(method) + '::>' + String(endpoint), { headers: headers, body: payload });
                });
            }

            return generateSocketEmitter;
        }()
    }, {
        key: 'generateFetchEmitter',
        value: function () {
            function generateFetchEmitter(method, endpoint, payload) {
                var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
                var namespace = this.options.namespace;

                var namespacePrefix = !!namespace ? '/' + String(namespace) : '';
                var path = '' + String(this.endpoint) + namespacePrefix + String(endpoint);
                return fetch(path, {
                    method: method.toUpperCase(),
                    body: JSON.stringify(payload),
                    headers: Object.assign({
                        'Content-Type': 'application/json'
                    }, headers)
                });
            }

            return generateFetchEmitter;
        }()
    }, {
        key: 'get',
        value: function () {
            function get(endpoint) {
                var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var headers = arguments[2];

                if (!!headers.http) {
                    return this.generateFetchEmitter('get', endpoint, payload, headers);
                }
                return this.generateSocketEmitter('get', endpoint, payload, headers);
            }

            return get;
        }()
    }, {
        key: 'post',
        value: function () {
            function post(endpoint) {
                var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var headers = arguments[2];

                if (!!headers.http) {
                    return this.generateFetchEmitter('post', endpoint, payload, headers);
                }
                return this.generateSocketEmitter('post', endpoint, payload, headers);
            }

            return post;
        }()
    }, {
        key: 'put',
        value: function () {
            function put(endpoint) {
                var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var headers = arguments[2];

                if (!!headers.http) {
                    return this.generateFetchEmitter('put', endpoint, payload, headers);
                }
                return this.generateSocketEmitter('put', endpoint, payload, headers);
            }

            return put;
        }()
    }, {
        key: 'delete',
        value: function () {
            function _delete(endpoint) {
                var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
                var headers = arguments[2];

                if (!!headers.http) {
                    return this.generateFetchEmitter('delete', endpoint, payload, headers);
                }
                return this.generateSocketEmitter('delete', endpoint, payload, headers);
            }

            return _delete;
        }()
    }, {
        key: 'listen',
        value: function () {
            function listen(url, callback) {
                var _options = this.options,
                    namespace = _options.namespace,
                    reactNative = _options.reactNative;

                var connectionOptions = {};
                this.endpoint = url;

                if (!!reactNative) connectionOptions.transports = ['websocket'];

                this.socket = _socket2['default'].connect(String(url) + '/' + String(namespace), connectionOptions);
                this.socket.on('connect', function () {
                    return !!callback && callback();
                });
                this.generateSocketioRoutes();
            }

            return listen;
        }()
    }]);

    return GlitrRouterClient;
}();

exports['default'] = GlitrRouterClient;

