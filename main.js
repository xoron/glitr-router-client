import io from 'socket.io-client';
import urlParser from 'url-parse';
import crypto from 'crypto';
import 'isomorphic-fetch';

class GlitrRouterClient {
    constructor(routes, options = {}) {
        const {
            namespace = '',
            requestTimeout = 10000,
            reactNative = false
        } = options;

        this.options = {
            namespace,
            requestTimeout
        };

        this.routes = routes;
        this.socket = null;
        this.endpoint = '';

        // required for react-native
        if (!!options.reactNative) window.navigator.userAgent = 'ReactNative';
    }

    generateSocketioRoutes() {
        this.routes.forEach(route => {
            this.socket.on(`${route.method}::>${route.path}`, this.generateSocketHandler(route));
        });
    }

    generateSocketHandler(route) {
        return (payload) => {
            const { body, headers } = payload;
            const req = {
                headers: {
                    ...headers,
                    ...urlParser(route.path)
                },
                body
            };

            const generateReponse = (status) => {
                return (data, headerProps = {}) => {
                    const newHeaders = {
                        status,
                        ...headers,
                        ...headerProps
                    }

                    if (!!headers.callback) {
                        this.socket.emit(headers.callback, { headers: newHeaders, body: data });
                    }
                }
            }

            const res = {
                send: generateReponse(200),
                end: generateReponse(200),
                emit: generateReponse(200),
                fail: generateReponse(400),
                error: generateReponse(200),
            };
    
            const runHandler = (index) => {
                route.handler[index](req, res, () => runHandler(index + 1));
            }
    
            runHandler(0);
        }
    }

    generateSocketEmitter(method, endpoint, payload, headers = {}) {
        return new Promise((resolve, reject) => {
            if (!!headers.callback) {
                const { requestTimeout } = this.options;
                const randomHash = crypto.randomBytes(64).toString('hex');

                const requestTimer = setTimeout(() => {
                    reject('request timed out :(');
                }, requestTimeout);
                
                headers.callback = randomHash;
                this.socket.once(randomHash, (payload) => {
                    clearTimeout(requestTimer);
                    resolve(payload)
                });
            }

            this.socket.emit(`${method}::>${endpoint}`, { headers, body: payload });
        });
    }

    generateFetchEmitter(method, endpoint, payload, headers = {}) {
        const { namespace } = this.options;
        const namespacePrefix = !!namespace ? `/${namespace}` : '';
        const path = `${this.endpoint}${namespacePrefix}${endpoint}`;
        return fetch(path, {
            method: method.toUpperCase(),
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json',
                ...headers,
            }
        });
    }

    get(endpoint, payload = {}, headers) {
        if (!!headers.http) {
            return this.generateFetchEmitter('get', endpoint, payload, headers);
        }
        return this.generateSocketEmitter('get', endpoint, payload, headers);
    }

    post(endpoint, payload = {}, headers) {
        if (!!headers.http) {
            return this.generateFetchEmitter('post', endpoint, payload, headers);
        }
        return this.generateSocketEmitter('post', endpoint, payload, headers);
    }

    put(endpoint, payload = {}, headers) {
        if (!!headers.http) {
            return this.generateFetchEmitter('put', endpoint, payload, headers);
        }
        return this.generateSocketEmitter('put', endpoint, payload, headers);
    }

    delete(endpoint, payload = {}, headers) {
        if (!!headers.http) {
            return this.generateFetchEmitter('delete', endpoint, payload, headers);
        }
        return this.generateSocketEmitter('delete', endpoint, payload, headers);
    }

    listen(url, callback) {
        const { namespace, reactNative } = this.options;
        const connectionOptions = {};
        this.endpoint = url;

        if (!!reactNative) connectionOptions.transports = ['websocket'];

        this.socket = io.connect(`${url}/${namespace}`, connectionOptions);
        this.socket.on('connect', () => !!callback && callback());
        this.generateSocketioRoutes();
    }
}

export default GlitrRouterClient;
