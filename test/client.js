import GlitrRouterClient from '..';

const routes = [
    {
        path: '/message',
        method: 'post',
        handler: [
            (req, res, next) => {
                console.log('socket middleware');
                next();
            },
            (req, res, next) => {
                console.log('socket middleware works as expected');
                next();
            },
            (req, res, next) => {
                console.log('socket middleware works with many middlewares!!!!');
                res.send();
            }
        ]
    },
    {
        path: '/response',
        method: 'get',
        handler: [
            (req, res, next) => {
                console.log('ting responser 1', req);
                next();
            },
            (req, res, next) => {
                console.log('socket middleware works as expected');
                next();
            },
            (req, res, next) => {
                console.log('socket middleware works with many middlewares!!!!');
                res.send();
            }
        ]
    }
];

const options = {
    namespace: 'testing',
    requestTimeout: 6000
};

const grClient = new GlitrRouterClient(routes, options);
grClient.listen('http://localhost:1234', () => {
    console.log('client is now connected');
});

// setTimeout(() => {
//     grClient.post('/da-ting', { headers: { callback: 'ting response', body: { something: 'in da bodyyyyy' }} }, { callback: true })
//         .then(data => console.log('success:', data))
//         .catch(e => console.log('ERROR:', e))

// }, 5000);