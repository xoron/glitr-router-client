import SocketIO from 'socket.io';
import http from 'http';
import express from 'express';

const app = express();
const server = http.createServer(app);
const io = new SocketIO(server);

io.of('testing').on('connection', socket => {
    console.log('someone connected')
    socket.emit('/router', {
        headers: {},
        body: {
            content: 'testing123'
        }
    });
});

server.listen(1234);
