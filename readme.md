# glitr-router-client

## example

`glitr-router-client` implements functionality on the a connected client to allow it to function as a router networked to the server. this means that the server is able to make requests similar to when a webapp makes a HTTP GET request to a server using some tool like isomorphic-fetch.

`./grClient.js`

```javascript
import GlitrRouterClient from '..';

const routes = [
    {
        path: '/route-on-client',
        method: 'post',
        handler: [
            (req, res, next) => {
                console.log('[client-side] router working');
                console.log('--- message from server');
                console.log(req.body);
                console.log('---');
                next();
            },
            (req, res, next) => {
                console.log('[client-side] socket middleware works as expected');
                res.send('message from CLIENT to SERVER in response');
            }
        ]
    }
];

const options = {
    namespace: 'testing', // optional: default = ''
    requestTimeout: 10000, // optional: default = 10000
    reactNative: false // required `true` for react-native
};

const grClient = new GlitrRouterClient(routes, options);
grClient.listen('http://localhost:1234', () => {
    console.log('client is now connected');
});

export default grClient;
```

the glitr-routes constructor takes 2 arguaments:

* routes
* options

### routes:

an array of objects:

|attribute|required|type|default|description
|--|--|--|--|--|
|path|required|string||the event name that socket.io-client should litening for. the socket will be set to listen to an event in the format: `${method}::>${path}`.|
|method|required|string. one of: `get`, `post`, `put`, `delete`||the method to use for transmitting the the message.|
|handler|required| function or array of functions||this is a function the takes in 3 parameters: `request`, `response`, `next`. the values passed into the function follow the same pattern as the middleware functions in [express route handlers](https://expressjs.com/en/guide/routing.html#route-handlers).

when creating `handler` methods for `socket.io`, if the request header `callback` property **from the server** is set to `true`, the handlers methods must respond back to the client.

the handler response object has a few helper methods to help with this:

|method|description|
|--|--|
|send| sends a response with a default status code `200`|
|end| sends a response with a default status code `200`|
|emit| sends a response with a default status code `200`|
|fail| sends a response with a default status code `400`|
|error| sends a response with a default status code `400`|

all these response methods take 2 parametes i.e. `res.send(data, headers)`. both parametes are options. `data` is the payload to send, `headers` is an object to pass additional data in the headers. you can assign a value to `headers.status` to sent to the client to ovverride the default status code mentioned in the previous table.

if the request takes longer than the timeout period specified in [options](#options), an exception will be thrown indicating timeout. you can handle this like a typical promise exception: `promise.catch(console.log)`.

### options

an object containing the following attributes:

|attrubute|required|default|description|
|--|--|--|--|
|namespace|optional|''|a namespace to assign to all the routes.|
|requestTimeout|optional|10000|this is the number of miliseconds to wait before throwing a timeout exception when making a request that should respond with a callback [(see below)](#request).|
|reactNative|required for react-native clients|false|set this value to determine if the client is using react-native.

## Usage

when a new instance of `glitr-router-client` is created, the instance exposes the following objects:

|attribute|description
|--|--|
|listen|this function takes 2 parameters: `url` and `callback`. the url should be a a string to represent the server endpoint to connect to (including namespaces).
|get|this is one of the HTTP event emitters (see below).|
|post|this is one of the HTTP event emitters (see below).|
|put|this is one of the HTTP event emitters (see below).|
|delete|this is one of the HTTP event emitters (see below).|

### HTTP event emitters

The event emitter methods mentioned in the previous table, are methods used to communicate with the server.

```javascript
import GlitrRouterClient from 'glitr-router-client';

const grClient = new GlitrRouterClient(routes, options);
grClient.listen('http://localhost:1234', () => {
    console.log('client is now connected');
});

// grClient.get(path, payload, headers)
// grClient.post(path, payload, headers)
// grClient.put(path, payload, headers)
// grClient.delete(path, payload, headers)
```

### Request

|name|required|type|description
|--|--|--|--|
|path|yes|string|a string the represents the path on the client to make the request.|
|payload|no|object|this optional first can be used for sending data to the server|
|headers|no|object|this is to pass additional metadata about the request.|
|headers.callback|no|boolean|set this value to true if you want to emit a message to the server and you want the server to respond when finished. (only relevent for socket endpoints on server. `express` expects this by default)|
|headers.http|no|boolean|set this value to determine if the message should be sent over `isomorphic-fetch`. the default behaviour is to make the request with `socket.io-client`.|

*when making a request through HTTP by specifying `headers.http = true`, `glitr-router-client` will use `isophomic-fetch` internally to make the request. if you want to specify http headers, simply add them to the headers object. to see what header options are available see [fetch docs](https://github.github.io/fetch/)*.

### Response

when `callback` is set to true in the headers of a request, the method will return a promise object with data sent back from the server like you would expect for any regular request over HTTP protocol.

when using `socket.io` to make the request, the response object properties are described in the following table.

|name|description|
|--|--|
|headers|some metadata about the request sent over by the server|
|body|this is the payload the server has sent back to the client.|

when using `express` to make the request, the response object is described on the [expressjs docs ](https://expressjs.com/en/4x/api.html#middleware-callback-function-examples)

### Example

```javascript
import grClient from './grClient';

grClient.post('/send-message',
    {
        message: 'hello world',
        recipient: 'clark'
    }, {
        callback: true
    }
).then((dataFromServer) => {
    // process dataFromServer here ...
    console.log(dataFromServer);
});
```

**for more details about the server-side implmentation see [glitr-router](https://www.npmjs.com/package/glitr-router)**.